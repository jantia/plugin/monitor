<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Exception;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface {

}
