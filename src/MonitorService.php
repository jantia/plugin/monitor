<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor;

//
use BackedEnum;
use Jantia\Plugin\Monitor\Std\MetricsPerformance;
use Jantia\Standard\Version\VersionPackageInterface;
use Jantia\Stdlib\Version\VersionPackage;
use Tiat\Standard\Monitor\MonitorInterface;

use function array_walk;
use function class_exists;
use function explode;
use function implode;
use function strtolower;
use function ucfirst;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class MonitorService extends AbstractMonitorService {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_VERSION_PACKAGE = VersionPackage::class;
	
	/**
	 * @var VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	private VersionPackageInterface $_packageInterface;
	
	/**
	 * @param ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(...$args) {
		if(( $p = $args['package'] ?? NULL ) !== NULL):
			$this->setPackageInterface($p);
		endif;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function start() : void {
		//
		if($this->hasStarted() === FALSE):
			//
			$this->setStarted(TRUE);
			
			//
			$this->setMonitoringTime(self::MONITORING_START, 'APPLICATION');
			
			// Get application module name & version
			if(( $name = $this->getName() ) !== NULL):
				$this->getPackageInterface()?->setName($name);
			endif;
			
			//
			$this->_result['package'] = $this->getMonitorName();
			
			// Resolve & start KPI's
			$this->_resolveKpi();
		endif;
	}
	
	/**
	 * @return null|VersionPackageInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getPackageInterface() : ?VersionPackageInterface {
		//
		if(empty($this->_packageInterface)):
			$this->setPackageInterface();
		endif;
		
		//
		return $this->_packageInterface ?? NULL;
	}
	
	/**
	 * @param    NULL|VersionPackageInterface    $interface
	 * @param                                    ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setPackageInterface(?VersionPackageInterface $interface = NULL, ...$args) : static {
		//
		if(empty($this->_packageInterface)):
			if($interface === NULL):
				$default                 = self::DEFAULT_VERSION_PACKAGE;
				$this->_packageInterface = new $default(...$args);
			else:
				$this->_packageInterface = $interface;
			endif;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getMonitorName() : ?array {
		return $this->getPackageInterface()?->toArray();
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _resolveKpi() : array {
		if(! empty($this->_metricValues)):
			foreach($this->_metricValues as $val):
				if(! empty($tmp = MetricsPerformance::getGroupValues($val))):
					foreach($tmp as $value):
						if($value instanceof BackedEnum):
							$this->_metrics[$value->value] = '';
						endif;
					endforeach;
				endif;
			endforeach;
			
			//
			return $this->_resolveKpiValues($this->_metrics);
		endif;
		
		//
		return [];
	}
	
	/**
	 * @param    array    $values
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _resolveKpiValues(array &$values = []) : array {
		//
		if(! empty($values)):
			foreach($values as $key => &$val):
				$tmp = explode('_', $key);
				array_walk($tmp, static function (&$value) {
					$value = ucfirst(strtolower($value));
				});
				$name  = implode('', $tmp);
				$class = self::KPI_VALUE_NAMESPACE . '\\' . $name;
				
				//
				if(class_exists($class) && ( $val = new $class() ) && $val instanceof MonitorInterface):
					if(strtolower($name) === 'cpu' || strtolower($name) === 'ram'):
						$this->_result['kpi'][strtolower($name)][self::MONITORING_START] = $val->getResult();
					else:
						$val->start();
					endif;
				endif;
			endforeach;
		endif;
		
		//
		return $values;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function stop() : void {
		//
		parent::stop();
		
		//
		if(! empty($this->_metrics)):
			foreach($this->_metrics as $key => $val):
				if($val instanceof MonitorInterface):
					//
					if(strtolower($key) === 'cpu' || strtolower($key) === 'ram'):
						$this->_result['kpi'][strtolower($key)][self::MONITORING_STOP] = $val->getResult();
					else:
						$val->stop();
						$this->_result['kpi'][$key] = $val->getResult();
					endif;
				endif;
			endforeach;
			
			//
			$this->setMonitoringTime(self::MONITORING_STOP, 'APPLICATION');
			$this->_result['application']['time'] = $this->getMonitoringTime('APPLICATION');
		endif;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetPackageInterface() : static {
		//
		unset($this->_packageInterface);
		
		//
		return $this;
	}
}
