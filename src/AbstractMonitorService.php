<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor;

//
use Tiat\Standard\Monitor\MonitorServiceInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractMonitorService extends AbstractMonitor implements MonitorServiceInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string KPI_VALUE_NAMESPACE = __NAMESPACE__ . "\Metrics\Value";
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string KPI_PERFORMANCE_NAMESPACE = __NAMESPACE__ . "\Metrics\Performance";
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_metrics = [];
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _resolveKpi() : array;
	
	/**
	 * @param    array    $values
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	abstract protected function _resolveKpiValues(array &$values = []) : array;
}
