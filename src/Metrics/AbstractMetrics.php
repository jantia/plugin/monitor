<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Metrics;

//
use Jantia\Plugin\Monitor\AbstractMonitor;

use function file_exists;
use function is_readable;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractMetrics extends AbstractMonitor implements MetricsInterface {
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_requiredFields;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_filename;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function start() : void {
	
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequiredFields() : array {
		return $this->_requiredFields ?? [];
	}
	
	/**
	 * Set required fields based to /proc/meminfo
	 *
	 * @param    array    $fields
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequiredFields(array $fields) : static {
		//
		$this->_requiredFields = $fields;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $filename
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setSourceFilename(string $filename) : static {
		//
		if(file_exists($filename) && is_readable($filename)):
			$this->_filename = $filename;
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Return source filename
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getSourceFilename() : ?string {
		return $this->_filename ?? NULL;
	}
}
