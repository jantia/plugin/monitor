<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Metrics\Performance;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class RateLimiter implements RateLimiterInterface {
	
	public function __construct() {
	
	}
	
	/**
	 * @return bool
	 */
	public function check() : bool {
		return FALSE;
	}
}
