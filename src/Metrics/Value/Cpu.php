<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Metrics\Value;

//
use Jantia\Plugin\Monitor\Exception\RuntimeException;
use Jantia\Plugin\Monitor\Metrics\AbstractMetrics;

use function array_sum;
use function count;
use function explode;
use function file_get_contents;
use function number_format;
use function preg_replace;
use function sprintf;
use function str_replace;
use function stripos;
use function strtolower;
use function sys_getloadavg;
use function trim;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Cpu extends AbstractMetrics {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	private const string SRC_FILENAME = '/proc/stat';
	
	/**
	 * @var array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_requiredFields = ['memTotal', 'memFree'];
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_cpuUsage;
	
	/**
	 * Get a decimal value as percentage of current CPU load
	 *
	 * @param    bool    $return
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult(bool $return = TRUE) : ?array {
		if(stripos(strtolower(PHP_OS_FAMILY), "linux") !== FALSE):
			return $this->_getCpuUsage($return);
		else:
			$msg = sprintf("Linux is only supported operating system family. Got '%s'.", PHP_OS_FAMILY);
			throw new RuntimeException($msg);
		endif;
	}
	
	/**
	 * @param    bool    $return
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	private function _getCpuUsage(bool $return = TRUE) : ?array {
		if($return === TRUE):
			//
			$result['avg']   = sys_getloadavg();
			$result['usage'] = $this->_getCpuInfo(TRUE);
			
			//
			return $result;
		else:
			return $this->_getCpuInfo();
		endif;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	private function _getCpuInfo(bool $return = FALSE) : ?float {
		if(! empty($stats = @file_get_contents($this->getSourceFilename() ?? self::SRC_FILENAME))):
			//
			$fixed = preg_replace("/[[:blank:]]+/", " ", $stats);
			
			$lines = str_replace(["\r\n", "\n\r", "\r"], "\n", $fixed);
			$array = explode("\n", $lines);
			
			foreach($array as $line):
				$tmp = explode(" ", trim($line));
				
				if(( count($tmp) >= 5 ) && ( $tmp[0] === "cpu" )):
					$usage = $tmp;
					break;
				endif;
			endforeach;
			
			// Sum up the 4 values for User, Nice, System and Idle and calculate
			// the percentage of idle time (which is part of the 4 values!)
			if(isset($usage)):
				//
				unset($usage[0]);
				
				//
				if($return === FALSE && empty($this->_cpuUsage)):
					$this->_cpuUsage = $usage;
				else:
					$old = array_sum($this->_cpuUsage ?? []);
					$new = array_sum($usage);
					
					// Delta between two reads
					$delta = $new - $old;
					
					//
					$idle = $usage[4] - ( $this->_cpuUsage[4] ?? 0 );
					$used = $delta - $idle;
					
					//
					return (float)number_format(( 100 * $used / $delta ), 2);
				endif;
			endif;
		endif;
		
		//
		return NULL;
	}
}
