<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Metrics\Value;

//
use Jantia\Plugin\Monitor\Exception\RuntimeException;
use Jantia\Plugin\Monitor\Metrics\AbstractMetrics;

use function array_merge;
use function explode;
use function file_get_contents;
use function floor;
use function memory_get_usage;
use function sprintf;
use function str_replace;
use function stripos;
use function strtolower;
use function trim;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Ram extends AbstractMetrics {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	private const string SRC_FILENAME = '/proc/meminfo';
	
	/**
	 * @var array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_requiredFields = ['memTotal', 'memFree'];
	
	/**
	 * Return used & allocated memory. Values are always kB
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : ?array {
		if(stripos(strtolower(PHP_OS_FAMILY), "linux") !== FALSE):
			return $this->_getMemoryUsage();
		else:
			$msg = sprintf("Linux is only supported operating system family. Got '%s'.", PHP_OS_FAMILY);
			throw new RuntimeException($msg);
		endif;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	private function _getMemoryUsage() : array {
		//
		$result['allocated']      = (int)floor(memory_get_usage() / 1024);
		$result['allocatedTotal'] = (int)floor(memory_get_usage(TRUE) / 1024);
		
		//
		if(( $meminfo = $this->_getMeminfo() ) !== NULL):
			return array_merge($result, $meminfo);
		endif;
		
		return $result;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	private function _getMeminfo() : ?array {
		if(! empty($stats = @file_get_contents($this->getSourceFilename() ?? self::SRC_FILENAME))):
			// Separate lines
			$lines = str_replace(["\r\n", "\n\r", "\r"], "\n", $stats);
			$array = explode("\n", $lines);
			
			//
			foreach($array as $line):
				if(! empty($line)):
					$tmp[] = explode(":", trim($line));
				endif;
			endforeach;
			
			//
			if(isset($tmp)):
				//
				foreach($tmp as $val):
					[$value] = explode(' ', trim($val[1]));
					$data[strtolower($val[0])] = $value;
				endforeach;
				
				//
				if(isset($data)):
					$fields = $this->getRequiredFields();
					foreach($fields as $index):
						foreach($data as $key => $val):
							if(strtolower($index) === $key):
								$result[$index] = $val;
							endif;
						endforeach;
					endforeach;
				endif;
			endif;
		endif;
		
		//
		return $result ?? NULL;
	}
}
