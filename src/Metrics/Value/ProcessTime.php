<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Metrics\Value;

//
use Jantia\Plugin\Monitor\Metrics\AbstractMetrics;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class ProcessTime extends AbstractMetrics {
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function start() : void {
		$this->setMonitoringTime(self::MONITORING_START, __CLASS__);
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function stop() : void {
		parent::stop();
		
		$this->setMonitoringTime(self::MONITORING_STOP, __CLASS__);
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getResult() : array {
		return ['time' => $this->getMonitorTime(TRUE, __CLASS__)];
	}
}
