<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Metrics;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface MetricsInterface {
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getRequiredFields() : array;
	
	/**
	 * @param    array    $fields
	 *
	 * @return MetricsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRequiredFields(array $fields) : MetricsInterface;
	
	/**
	 * @param    string    $filename
	 *
	 * @return MetricsInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setSourceFilename(string $filename) : MetricsInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getSourceFilename() : ?string;
}
