<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;
use Tiat\Standard\Exception\InvalidArgumentException;

use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum MetricsFactor: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case RELIABILITY = 'RELIABILITY';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SCALABILITY = 'SCALABILITY';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PERFORMANCE = 'PERFORMANCE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SECURITY = 'SECURITY';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case MAINTAINABILITY = 'MAINTAINABILITY';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case RATE_OF_DELIVERY = 'RATE_OF_DELIVERY';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case TESTABILITY = 'TESTABILITY';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case USABILITY = 'USABILITY';
	
	/**
	 * @param    MetricsFactor    $factor
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public static function getGroup(MetricsFactor $factor) : array {
		return match ( $factor ) {
			self::RELIABILITY => [MetricsValues::EXCEPTIONS],
			self::PERFORMANCE => [MetricsValues::PROCESS_TIME, MetricsValues::NUMBER_OF_TRANSACTIONS,
			                      MetricsValues::THROUGHPUT],
			self::SECURITY => [MetricsValues::ERRORS, MetricsValues::EXCEPTIONS],
			self::USABILITY => [MetricsValues::WAITING_TIME, MetricsValues::RESPONSE_TIME],
			default => throw new InvalidArgumentException(sprintf("No handler for %s", $factor->value))
		};
	}
}
