<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Std;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface MonitorTimeInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string MONITORING_START = 'MONITORING_START';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string MONITORING_STOP = 'MONITORING_STOP';
	
	/**
	 * @param    string    $status
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setMonitoringTime(string $status, string $name) : void;
	
	/**
	 * @param    bool    $asNumber
	 *
	 * @return array|float|false|int
	 * @since   3.0.0 First time introduced.
	 */
	public function getMonitorTime(bool $asNumber = FALSE) : array|float|false|int;
	
	/**
	 * @param    string    $name
	 *
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getMonitoringTime(string $name) : int;
}
