<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * KPI values for metrics.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/metrics
 */
enum MetricsValues: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * Cache hit %
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CACHING_RATIO = 'CACHING_RATIO';
	
	/**
	 * CPU usage
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case CPU = 'CPU';
	
	/**
	 * Disk usage
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case DISK_USAGE = 'DISK_USAGE';
	
	/**
	 * Amount of errors
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case ERRORS = 'ERRORS';
	
	/**
	 * Amount of exceptions
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case EXCEPTIONS = 'EXCEPTIONS';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case HITS_PER_SECOND = 'HITS_PER_SECOND';
	
	/**
	 * Latency time (ms)
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case LATENCY = 'LATENCY';
	
	/**
	 * Amount of transactions processed
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case NUMBER_OF_TRANSACTIONS = 'NUMBER_OF_TRANSACTIONS';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PER_SECOND = 'PER_SECOND';
	
	/**
	 * Processing time (total, ms)
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PROCESS_TIME = 'PROCESS_TIME';
	
	/**
	 * Ram usage
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case RAM = 'RAM';
	
	/**
	 * Response time
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case RESPONSE_TIME = 'RESPONSE_TIME';
	
	/**
	 * Request(s) per second
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case REQUEST_PER_SECOND = 'REQUEST_PER_SECOND';
	
	/**
	 * User session length (seconds)
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case SESSION_LENGTH = 'SESSION_LENGTH';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SYSTEM_LOAD = 'SYSTEM_LOAD';
	
	/**
	 * Bytes per second
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case THROUGHPUT = 'THROUGHPUT';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case USER_AMOUNT = 'USER_AMOUNT';
	
	/**
	 * Waiting time
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case WAITING_TIME = 'WAITING_TIME';
}
