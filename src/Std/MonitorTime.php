<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Std;

//
use Jantia\Plugin\Monitor\Exception\InvalidArgumentException;

use function hrtime;
use function sprintf;

/**
 * Monitoring time tools. Final result is calculated as milliseconds.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait MonitorTime {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_MONITOR_PATTERN = 'DEFAULT';
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_monitorTimes;
	
	/**
	 * @param    string    $status
	 * @param    string    $name
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setMonitoringTime(string $status, string $name = self::DEFAULT_MONITOR_PATTERN) : void {
		//
		if($status === MonitorTimeInterface::MONITORING_START || $status === MonitorTimeInterface::MONITORING_STOP):
			if(! empty($name)):
				$this->_monitorTimes[$name][$status] = $this->getMonitorTime(FALSE);
			else:
				// Throw exception because we must have the name for pattern
				throw new InvalidArgumentException("Name value can't be empty and must have the value.");
			endif;
		else:
			// Throw exception because we must have valid value
			$msg = sprintf("Status value must be either '%s' or '%s', '%s' given.", self::MONITORING_START,
			               self::MONITORING_STOP, $status);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    bool    $asNumber
	 *
	 * @return array|false|float|int|int[]
	 * @since   3.0.0 First time introduced.
	 */
	public function getMonitorTime(bool $asNumber = FALSE) : array|float|false|int {
		return hrtime($asNumber);
	}
	
	/**
	 * Result is in milliseconds.
	 *
	 * @param    string    $name
	 *
	 * @return int
	 * @since   3.0.0 First time introduced.
	 */
	public function getMonitoringTime(string $name = self::DEFAULT_MONITOR_PATTERN) : int {
		//
		if(! empty($name)):
			$start = $this->_monitorTimes[$name][MonitorTimeInterface::MONITORING_START];
			$end   = $this->_monitorTimes[$name][MonitorTimeInterface::MONITORING_STOP] ?? $this->getMonitorTime(FALSE);
			
			//
			$t1 = ( $start[0] + $start[1] ) / 1000000000;
			$t2 = ( $end[0] + $end[1] ) / 1000000000;
			
			//
			return (int)( ( $t2 - $t1 ) * 1000 );
		endif;
		
		//
		throw new InvalidArgumentException("Pattern name can't be empty.");
	}
}
