<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor\Std;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * Define KPI types for metrics.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/docs/tiat/metrics
 */
enum MetricsPerformance: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ENDURANCE = 'ENDURANCE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case LOAD = 'LOAD';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case PERFORMANCE = 'PERFORMANCE';
	
	/**
	 * Use this as the basic metrics monitoring for the application. This will include all base KPI values for safe monitoring.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	case PROCESS = 'PROCESS';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SCALABILITY = 'SCALABILITY';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case SPIKE = 'SPIKE';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case STRESS = 'STRESS';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case VOLUME = 'VOLUME';
	
	/**
	 * @param    MetricsPerformance    $performance
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public static function getGroupValues(MetricsPerformance $performance) : array {
		return match ( $performance ) {
			self::ENDURANCE, self::SCALABILITY, self::SPIKE => [],
			self::LOAD, self::STRESS => [MetricsValues::THROUGHPUT, MetricsValues::RESPONSE_TIME],
			self::PERFORMANCE => [MetricsValues::HITS_PER_SECOND, MetricsValues::ERRORS, MetricsValues::RESPONSE_TIME,
			                      MetricsValues::LATENCY, MetricsValues::THROUGHPUT],
			self::PROCESS => [MetricsValues::CPU, MetricsValues::THROUGHPUT, MetricsValues::ERRORS,
			                  MetricsValues::CACHING_RATIO, MetricsValues::DISK_USAGE, MetricsValues::EXCEPTIONS,
			                  MetricsValues::RAM, MetricsValues::WAITING_TIME, MetricsValues::PROCESS_TIME],
			self::VOLUME => [MetricsValues::REQUEST_PER_SECOND, MetricsValues::THROUGHPUT],
		};
	}
}
