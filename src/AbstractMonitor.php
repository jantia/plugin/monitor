<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Monitor
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Monitor;

//
use Jantia\Plugin\Monitor\Exception\RuntimeException;
use Jantia\Plugin\Monitor\Std\MetricsPerformance;
use Jantia\Plugin\Monitor\Std\MonitorTime;
use Jantia\Plugin\Monitor\Std\MonitorTimeInterface;
use Override;
use Tiat\Standard\Monitor\MonitorInterface;

use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractMonitor implements MonitorInterface, MonitorTimeInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use MonitorTime;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_metricValues = [MetricsPerformance::PROCESS];
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_result;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private readonly bool $_hasStarted;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private readonly bool $_hasStopped;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_nameMonitor;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	abstract public function start() : void;
	
	/**
	 * @param    bool    $status
	 *
	 * @return MonitorInterface
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function setStarted(bool $status) : MonitorInterface {
		//
		$this->_hasStarted = $status;
		
		//
		return $this;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function stop() : void {
		// Sometimes this can be called multiple times so prevent it
		if($this->hasStarted() === TRUE && $this->hasStopped() === FALSE):
			$this->setStopped(TRUE);
		endif;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function hasStarted() : bool {
		return $this->_hasStarted ?? FALSE;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function hasStopped() : bool {
		return $this->_hasStopped ?? FALSE;
	}
	
	/**
	 * @param    bool    $status
	 *
	 * @return MonitorInterface
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function setStopped(bool $status) : MonitorInterface {
		//
		$this->_hasStopped = $status;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string|array|int|false
	 * @since   3.0.0 First time introduced.
	 */
	#[Override] public function getResult() : string|array|int|false|null {
		return $this->_result ?? NULL;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetResult() : void {
		unset($this->_result);
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getName() : ?string {
		return $this->_nameMonitor ?? NULL;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setName(string $name) : static {
		//
		$this->_nameMonitor = $name;
		
		//
		return $this;
	}
	
	/**
	 * Check data collection status & that the monitoring is stopped.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __destruct() {
		if($this->hasStarted() === TRUE && $this->hasStopped() === FALSE):
			$msg =
				sprintf("Monitoring has been started but never stopped on %s. Please collect data and stop monitoring.",
				        __METHOD__);
			throw new RuntimeException($msg);
		endif;
	}
}
